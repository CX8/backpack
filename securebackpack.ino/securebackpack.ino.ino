#include<Wire.h>
#include <SoftwareSerial.h>


//variables
const int diff = 2000;
int16_t x, y, z; 
const int MPU_addr=0x68;
int16_t AcX,AcY,AcZ,Tmp,GyX,GyY,GyZ;
SoftwareSerial BTSerial(7, 6); // RX | TX
char data = '0';
const int pinReed1 = A0;
const int pinReed2 = A1;
const int pinReed3 = A2;
const int pinReed4 = A3;
const int buzzer = 10;
const int steps = 10;
const int increase = 10;
const int lowerfreq = 2000;
const int lowfreq = 4000;
const int highfreq = lowfreq + increase*steps;
bool alarm = false;
bool danger = false;
const int play = 25;
const int pause = play-1;


void setup() 
{
  Wire.begin();
  Wire.beginTransmission(MPU_addr);
  Wire.write(0x6B);
  Wire.write(0);
  Wire.endTransmission(true);
  Serial.begin(9600);
  BTSerial.begin(115200);

 readAcc();
  x=AcX;
  y=AcY;
  z=AcZ;

  //wire stuff by
  //Written by Ahmet Burkay KIRNIK
  //TR_CapaFenLisesi
}

void loop() 
{
  readAcc();
  Serial.println(AcX);
  Serial.println(AcY);
  Serial.println(AcZ);

  
  if (BTSerial.available()) 
  {
    data = BTSerial.read();
    Serial.println(data);
    if (data == 'a')
    {
      alarm =  true;
      activateAlarm();
    }else if(data =='o')
    {
      alarm = false;
      danger = false;
      deactivateAlarm();
    }

  }
  
  if (alarm)
  {
    int magnetRead1 = analogRead(pinReed1);
    int magnetRead2 = analogRead(pinReed2);
    int magnetRead3 = analogRead(pinReed3);
    int magnetRead4 = analogRead(pinReed4);
    //Serial.println(magnetRead);
    //more reeed switches, use interrupts maybe?
    if ((magnetRead1 <= 50) || (magnetRead2 <= 50) || (magnetRead3 <= 50) || (magnetRead4 <= 50))// or ble disconnected (someone running out of reach)
    //maybe if not possible to see connection status, send a char, and wait for anwer from phon
    //no anwer in 2-3 sec call gg and explode 
    {
      BTSerial.println("hello");//change to allarm
      soundAlarm();
    }

    if(abs(AcX - x) > diff){
      soundAlarm();
    Serial.println("danger X");
  }
    if(abs(AcY - y) > diff){
      soundAlarm();
    Serial.println("danger y");
  }
    if(abs(AcZ - z) > diff){
      soundAlarm();
    Serial.println("danger z");
  }
  }
  if(danger){
    soundAlarm();
    }

  x=AcX;
  y=AcY;
  z=AcZ;
}

void readAcc(){
  Wire.beginTransmission(MPU_addr);
  Wire.write(0x3B);
  //#MPU6050_ACCEL_XOUT_H 0x3B
  //...
//#define MPU6050_ACCEL_XOUT_L 0x3C // R 
//#define MPU6050_ACCEL_YOUT_H 0x3D // R 
//#define MPU6050_ACCEL_YOUT_L 0x3E // R 
//#define MPU6050_ACCEL_ZOUT_H 0x3F // R 
//#define MPU6050_ACCEL_ZOUT_L 0x40 // R  
  Wire.endTransmission(false);
  Wire.requestFrom(MPU_addr,14,true);
  AcX=Wire.read()<<8|Wire.read();
  AcY=Wire.read()<<8|Wire.read();
  AcZ=Wire.read()<<8|Wire.read();
  }

void soundAlarm() 
{
  
  if (!danger){
    BTSerial.println("hello");//change to allarm
    danger = true;
    }
  tone(buzzer, 3000, 500);
  delay(1000);
}

void activateAlarm()
{
  for (int i = 0; i<steps; i++){
    tone(buzzer, lowfreq + i*increase, play);
    delay(pause);
    }

  delay(200);
   for (int i = 0; i<steps; i++){
    tone(buzzer, lowfreq + i*increase, play);
    delay(pause);
    }
}

void deactivateAlarm()
{
    for (int i = 0; i<steps; i++){
    tone(buzzer, lowerfreq - i*increase, play);
    delay(pause);
    }

  delay(200);
   for (int i = 0; i<steps; i++){
    tone(buzzer, lowerfreq - i*increase, play);
    delay(pause);
    }  
}

