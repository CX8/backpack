package com.example.christianvuerich.myapplication;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.UUID;

public class MainActivity extends AppCompatActivity
{
    TextView myLabel;
    BluetoothAdapter mBluetoothAdapter;
    BluetoothSocket mmSocket;
    OutputStream mmOutputStream;
    InputStream mmInputStream;
    Thread workerThread;
    Boolean allarm = false;
    static final UUID myUUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
    byte[] readBuffer;
    int readBufferPosition;
    volatile boolean stopWorker;
    String address = null;
    Button btnOn, btnDis;
    Boolean allarmOn = false;


    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_led_control);
        myLabel = (TextView) findViewById(R.id.label);

        new ConnectBT().execute(); //Call the class to connect

        Intent newint = getIntent();
        address = newint.getStringExtra(DeviceList.EXTRA_ADDRESS); //receive the address of the bluetooth device

        setContentView(R.layout.activity_led_control);

        btnOn = (Button) findViewById(R.id.button2);
        btnDis = (Button) findViewById(R.id.button4);
        btnOn.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                toggleAlarm();      //method to turn on
            }
        });
        btnOn.setBackgroundColor(Color.GREEN);
        btnDis.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Disconnect(); //close connection
            }
        });


    }

    private void Disconnect()
    {
        if (mmSocket != null) //If the btSocket is busy
        {
            try
            {
                closeBT();
            }
            catch (Exception e)
            {
                System.out.println("disconnect fail");
                finish();
            }
        }

        finish(); //return to the first layout

    }


    private void toggleAlarm()
    {
        if (mmSocket != null)
        {
            try
            {
                if(!allarmOn) {
                    mmSocket.getOutputStream().write("a".getBytes());
                    beginListenForData();
                    allarmOn =true;
                    btnOn.setText("Deactivate");
                    btnOn.setBackgroundColor(Color.RED);
                }else{
                    mmSocket.getOutputStream().write("o".getBytes());
                    allarmOn=false;
                    btnOn.setText("Activate");
                    btnOn.setBackgroundColor(Color.GREEN);
                }

            }
            catch (Exception e)
            {
                System.out.println("ff");
                finish();
            }
        }
    }


    void beginListenForData()
    {
        final Handler handler = new Handler();
        final byte delimiter = 10; //This is the ASCII code for a newline character
        stopWorker = false;
        readBufferPosition = 0;
        readBuffer = new byte[1024];

        workerThread = new Thread(new Runnable()
        {
            public void run()
            {
                while (!Thread.currentThread().isInterrupted() && !stopWorker)
                {
                    try
                    {
                        int bytesAvailable = mmInputStream.available();

                        if (bytesAvailable > 0)
                        {
                            byte[] packetBytes = new byte[bytesAvailable];
                            mmInputStream.read(packetBytes);

                            for (int i = 0; i < bytesAvailable; i++)
                            {
                                byte b = packetBytes[i];

                                if (b == delimiter)
                                {
                                    byte[] encodedBytes = new byte[readBufferPosition];
                                    System.arraycopy(readBuffer, 0, encodedBytes, 0, encodedBytes.length);
                                    final String data = new String(encodedBytes, "US-ASCII");
                                    readBufferPosition = 0;

                                    handler.post(new Runnable()
                                    {
                                        public void run()
                                        {
                                            System.out.println(data);

                                            if(!allarm)
                                            {
                                                allarm=true;

                                                new AlertDialog.Builder(MainActivity.this)
                                                        .setTitle("Your backpack is compromised")
                                                        .setMessage("Go check it!")
                                                        .setPositiveButton("Deactivate", new DialogInterface.OnClickListener()
                                                        {
                                                            public void onClick(DialogInterface dialog, int which)
                                                            {
                                                                allarm=false;
                                                                toggleAlarm();
                                                            }
                                                        })
                                                        .setNegativeButton("Dismiss", new DialogInterface.OnClickListener()
                                                        {
                                                            public void onClick(DialogInterface dialog, int which)
                                                            {
                                                                // do nothing
                                                            }
                                                        })
                                                        .setIcon(android.R.drawable.ic_dialog_alert)
                                                        .show();
                                            }
                                        }
                                    });
                                }
                                else {
                                    readBuffer[readBufferPosition++] = b;
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        stopWorker = true;
                        finish();
                    }
                }
            }
        });

        workerThread.start();
    }


    void closeBT() throws Exception {
        stopWorker = true;

        if(mmOutputStream!=null)
            mmOutputStream.close();

        if(mmInputStream!=null)
            mmInputStream.close();

        if(mmSocket!=null)
            mmSocket.close();

        myLabel.setText("Bluetooth Closed");
    }


    @Override
    public void onStart()
    {
        super.onStart();
    }

    @Override
    public void onStop()
    {
        try
        {

            closeBT();
        }
        catch (Exception ex)
        {
            finish();
        }

        super.onStop();
    }

    private class ConnectBT extends AsyncTask<Void, Void, Void>  // UI thread
    {
        private boolean ConnectSuccess = true; //if it's here, it's almost connected


        @Override
        protected Void doInBackground(Void... devices) //while the progress dialog is shown, the connection is done in background
        {
            try
            {
                if (mmSocket == null)
                {
                    mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();//get the mobile bluetooth device
                    BluetoothDevice dispositivo = mBluetoothAdapter.getRemoteDevice(address);//connects to the device's address and checks if it's available
                    mmSocket = dispositivo.createInsecureRfcommSocketToServiceRecord(myUUID);//create a RFCOMM (SPP) connection
                    BluetoothAdapter.getDefaultAdapter().cancelDiscovery();
                    mmSocket.connect();//start connection
                    mmOutputStream = mmSocket.getOutputStream();
                    mmInputStream = mmSocket.getInputStream();
                }
            }
            catch (Exception e) {
                ConnectSuccess = false;//if the try failed, you can check the exception here
                finish();

            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) //after the doInBackground, it checks if everything went fine
        {
            super.onPostExecute(result);

            if (!ConnectSuccess)
            {
                finish();
            }
            else {
            }
        }
    }


}